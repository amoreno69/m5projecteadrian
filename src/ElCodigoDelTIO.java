import java.util.*;

/**
 * <h2>Clase per resoldre el problema del DomJutge AdrianMoreno</h2>
 * 
 * @version 2020
 * @author Adrian Moreno
 * @since 28/01/2020
 */

public class ElCodigoDelTIO {
	
	/**
	 * Array per guardar els diferents resultats
	 */
	static int[] mem = new int[1006];
	
	/**
	 * Variable global Resultat de tota la consulta
	 */
    static int res = 0;
    
    /**
     * M�tode de recollida de par�metres com el codi total i els diferents codis.
     * Crida a la funcio solve i fa print de les combinacions
     * @param args Inputs al programa
     */
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int n1 = -1;
		while(n1!=0) {
    		n1 = scn.nextInt();
    		if(n1!=0) {
	    		String[] codigos = new String[n1];
	    		for(int i = 0;i<n1;i++) codigos[i] = scn.next();
	    		String tot = scn.next();
	    		System.out.println(solve(tot,codigos));
    		}
		} scn.close();
	}
    
    /**
     * M�tode que resol el problema recursivament
     * @param actu String la qual esta mirant ara mateix (inicialment la mateixa que actu)
     * @param tot String que vols consultar
     * @param datos Array d'Strings de tots els codis diferents
     * @return Totes les combinacions dels codis per formar la paraula actu
     */
    public static int DP(String actu, String tot, String[] datos) {
    	res = 0;
    	if(actu.length()==0) return 1; //Si la paraula ha estat completament esborrada retorna 1 combinacio
    	while(actu.charAt(0)=='0')actu=actu.substring(1); //Elimina els zeros de la paraula si hi son al principi
    	if(mem[actu.length()]!=-1)return mem[actu.length()]; //Si ja has estat en aquesta posicio i n'hi ha un resultat a la memoria retorna el resultat
    	for(int i = 0;i<datos.length;i++) {
    		if(datos[i].length()<=actu.length()) { //Si el codi no es m�s gran que la paraula actual
    			if(datos[i].equals(actu.substring(0,datos[i].length()))) { //Si el codi coincideix amb una part de la paraula actual
    				res+=DP(actu.substring(datos[i].length()),tot,datos)%1000000007; //Retorna quantes combinacions hi ha de la paraula sense aquesta part de codi
    			}
    		}
    	}
    	return mem[actu.length()] = res%1000000007; //Retorna el resultat actual modul de 1000000007
    }
	
	/**
	 * Funcio que reincialitza la memoria i crida a la funci� DP per fer les diferents combinacions
	 * @param palab Codi total el qual voles saber les combinacions
	 * @param datos Els diferents codis per fer les combinacions
	 * @return Les combinacions dels codis
	 */
	public static int solve (String palab, String[] datos) {
		for(int i = 0;i<palab.length()+1;i++) mem[i] = -1; //Reinicialitza la memoria
		return (DP(palab, palab, datos)%1000000007);
	}
	
}
