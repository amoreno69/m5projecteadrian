import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Test1 {

	@Test
	void testPrograma1() {
		int res = 135;
		int n = ElCodigoDelTIO.solve("2112221121212221212222111222", new String[] {"1", "2", "22"});
		assertEquals(res,  n);
		res = 1;
		n = ElCodigoDelTIO.solve("101010101111101110101011", new String[] {"1"});
		assertEquals(res,  n);
		res = 0;
		n = ElCodigoDelTIO.solve("98979887999899898", new String[] {"77", "2", "44", "3"});
		assertEquals(res,  n);
	}

}
